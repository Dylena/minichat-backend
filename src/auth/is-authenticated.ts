import passport from 'fastify-passport';

export const isAuthenticated = passport.authenticate('local', async (_request, _reply) => {
  console.log('user', _request);
  if (_request.user) {
    return Promise.resolve();
  }
  return _reply.status(401).send('Unauthorized');
});
