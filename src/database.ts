import { Pool } from 'pg';

export const createDb = async (connectionString: string) => {
  return new Pool({ connectionString });
};
