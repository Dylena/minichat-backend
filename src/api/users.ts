// /* eslint-disable @typescript-eslint/no-floating-promises */
import { FastifySchema } from 'fastify';
import passport from 'fastify-passport';

import bcrypt from 'bcrypt';
import { RouterReturn } from '../interfaces/router-return';
import { UsersRepo } from '../repos/users.repo';
import { isAuthenticated } from '../auth/is-authenticated';
import { IUser } from '../interfaces/user';

export const createUsersRouter = (usersRepo: UsersRepo): RouterReturn => {
  return (fastify, _, done) => {
    fastify.post(
      '/login',
      {
        schema: {
          description: 'Login',
          summary: 'Login',
          tags: ['user'],
          body: {
            type: 'object',
            properties: {
              email: { type: 'string' },
              password: { type: 'string' },
            },
          },
          response: {
            200: {
              type: 'object',
              properties: {
                status: { type: 'string' },
              },
            },
          },
        } as FastifySchema,
        preHandler: passport.authenticate('local', { authInfo: false }),
      },
      async (_request, _reply) => {
        return _reply.status(200).send({ status: 'Logged in' });
      },
    );

    fastify.get<{ Params: { id: string } }>(
      '/me',
      {
        schema: {
          description: 'User',
          summary: 'Get user',
          tags: ['user'],
          response: {
            200: {
              type: 'object',
              properties: {
                name: { type: 'string' },
                email: { type: 'string' },
                id: { type: 'number' },
              },
            },
          },
        } as FastifySchema,
        preValidation: isAuthenticated,
      },
      async (_request, _reply) => {
        console.log('valami', _request.user);
        const { id } = _request.user as IUser;
        const result = await usersRepo.getUserById(id);
        console.log(result);
        return _reply.status(200).send(result);
      },
    );

    fastify.get(
      '/users',
      {
        schema: {
          description: 'Users',
          summary: 'Get users',
          tags: ['user'],
          response: {
            200: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  name: { type: 'string' },
                  email: { type: 'string' },
                  id: { type: 'number' },
                },
              },
            },
          },
        } as FastifySchema,
        preValidation: isAuthenticated,
      },
      async (_request, _reply) => {
        const result = await usersRepo.getUsers();
        console.log(result);
        return _reply.status(200).send(result);
      },
    );

    fastify.delete(
      '/logout',
      {
        schema: {
          description: 'Logged out',
          summary: 'Logged out',
          tags: ['user'],
          response: {
            200: {
              type: 'object',
              properties: {
                status: { type: 'string' },
              },
            },
          },
        } as FastifySchema,
      },
      async (_request, _reply) => {
        _request.session.delete();
        return _reply.status(200).send({ status: 'Logged out' });
      },
    );

    fastify.post<{ Body: { name: string; email: string; password: string } }>(
      '/signup',
      {
        schema: {
          description: 'Signup',
          summary: 'Signup',
          tags: ['user'],
          body: {
            type: 'object',
            properties: {
              name: { type: 'string' },
              email: { type: 'string' },
              password: { type: 'string' },
            },
          },
          response: {
            200: {
              type: 'object',
              properties: {
                id: { type: 'number' },
                name: { type: 'string' },
                email: { type: 'string' },
              },
            },
          },
        } as FastifySchema,
      },
      async (_request, _reply) => {
        const { name, email, password } = _request.body;
        const saltRounds = 10;
        const salt = await bcrypt.genSalt(saltRounds);
        const passwordHash = await bcrypt.hash(password, salt);
        const user = await usersRepo.createUser(name, email, passwordHash, salt);
        return _reply.status(200).send({ id: user.id, email: user.email });
      },
    );

    done();
  };
};
