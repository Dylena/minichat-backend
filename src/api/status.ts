// /* eslint-disable @typescript-eslint/no-floating-promises */
import { FastifySchema } from 'fastify';
import { isAuthenticated } from '../auth/is-authenticated';
import { RouterReturn } from '../interfaces/router-return';

export const createStatusRouter = (): RouterReturn => {
  return (fastify, _, done) => {
    fastify.get(
      '/status',
      {
        schema: {
          description: 'Status',
          summary: 'Status',
          tags: ['status'],
          response: {
            200: {
              type: 'object',
              properties: {
                status: { type: 'string' },
              },
            },
          },
        } as FastifySchema,
        preValidation: isAuthenticated,
      },
      async (_request, _reply) => {
        return _reply.status(200).send('Ok');
      },
    );

    done();
  };
};
