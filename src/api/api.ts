import { RouterReturn } from '../interfaces/router-return';
import { MessagesRepo } from '../repos/messages.repo';
import { RoomsRepo } from '../repos/rooms.repo';
import { UsersRepo } from '../repos/users.repo';
import { createMessagesRouter } from './messages';
import { createRoomsRouter } from './rooms';
import { createStatusRouter } from './status';
import { createUsersRouter } from './users';

export const createRouter = (repos: {
  roomsRepo: RoomsRepo;
  messagesRepo: MessagesRepo;
  usersRepo: UsersRepo;
}): RouterReturn => {
  return async (fastify, _, done) => {
    const roomsRouter = createRoomsRouter(repos.roomsRepo);
    await fastify.register(roomsRouter, { prefix: '/rooms' });

    const messagesRouter = createMessagesRouter(repos.messagesRepo);
    await fastify.register(messagesRouter, { prefix: '/rooms' });

    const usersRouter = createUsersRouter(repos.usersRepo);
    await fastify.register(usersRouter);

    const statusRouter = createStatusRouter();
    await fastify.register(statusRouter);

    done();
  };
};
