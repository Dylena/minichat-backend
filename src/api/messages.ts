// /* eslint-disable @typescript-eslint/no-floating-promises */
import { FastifySchema } from 'fastify';
import { IMessage } from '../interfaces/message';
import { RouterReturn } from '../interfaces/router-return';
import { MessagesRepo } from '../repos/messages.repo';

export const createMessagesRouter = (messagesRepo: MessagesRepo): RouterReturn => {
  return (fastify, _, done) => {
    fastify.get<{ Params: { roomId: number } }>(
      '/:roomId/messages',
      {
        schema: {
          description: 'Get messages by roomId',
          summary: 'Get messages by roomId',
          tags: ['rooms'],
          params: {
            type: 'object',
            properties: {
              id: {
                type: 'string',
                description: 'roomId',
              },
            },
          },
          response: {
            200: {
              type: 'array',
              description: 'Successful response',
              items: {
                properties: {
                  id: { type: 'number' },
                  sender: { type: 'string' },
                  data: { type: 'string' },
                  date: { type: 'string' },
                },
              },
            },
          },
        } as FastifySchema,
      },
      async (_request, _reply) => {
        const { roomId } = _request.params;
        const room = await messagesRepo.get(roomId);
        if (!room) {
          return _reply.status(404).send('Not Found');
        }
        return _reply.status(200).send(room.rows);
      },
    );

    fastify.post(
      '/:roomId/messages',
      {
        schema: {
          description: 'Post message to room',
          summary: 'Post message to room',
          tags: ['rooms'],
          params: {
            type: 'object',
            properties: {
              id: {
                type: 'number',
                description: 'roomId',
              },
            },
          },
          body: {
            type: 'object',
            properties: {
              data: { type: 'string' },
              sender: { type: 'string' },
            },
          },
          response: {
            200: {
              type: 'object',
              description: 'Successful response',
              properties: {
                id: { type: 'number' },
                data: { type: 'string' },
                date: { type: 'string' },
                sender: { type: 'string' },
              },
            },
          },
        } as FastifySchema,
      },
      async (_request, _reply) => {
        const { roomId } = _request.params as { roomId: number };
        const message = _request.body as IMessage;
        const result = await messagesRepo.add(roomId, message);
        if (!result) {
          return _reply.status(503).send('Could not create message');
        }
        fastify.io.emit('chat', message);
        return _reply.status(200).send(result);
      },
    );
    done();
  };
};
