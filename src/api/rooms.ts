// /* eslint-disable @typescript-eslint/no-floating-promises */
import { FastifySchema } from 'fastify';
import { RouterReturn } from '../interfaces/router-return';
import { RoomsRepo } from '../repos/rooms.repo';

export const createRoomsRouter = (roomsRepo: RoomsRepo): RouterReturn => {
  return (fastify, _, done) => {
    fastify.get(
      '/',
      {
        schema: {
          description: 'Get all rooms',
          summary: 'Get all rooms',
          tags: ['rooms'],
          response: {
            200: {
              type: 'array',
              description: 'Successful response',
              items: {
                type: 'object',
                properties: {
                  id: { type: 'number' },
                  name: { type: 'string' },
                },
              },
            },
          },
        } as FastifySchema,
      },
      async (_request, _reply) => {
        const rooms = await roomsRepo.list();
        if (!rooms.rows.length) {
          return _reply.status(404).send('Not Found');
        }
        return _reply.send(rooms.rows);
      },
    );

    fastify.get<{ Params: { roomId: string } }>(
      '/:roomId',
      {
        schema: {
          description: 'Get room by Id',
          summary: 'Get room by Id',
          tags: ['rooms'],
          response: {
            200: {
              type: 'object',
              description: 'Successful response',
              properties: {
                id: { type: 'number' },
                name: { type: 'string' },
              },
            },
          },
        } as FastifySchema,
      },
      async (_request, _reply) => {
        const { roomId } = _request.params;
        const room = await roomsRepo.get(roomId);
        return _reply.send(room.rows[0]);
      },
    );

    fastify.post(
      '/',
      {
        schema: {
          description: 'Post room',
          summary: 'Post room',
          tags: ['rooms'],
          body: {
            type: 'object',
            properties: {
              name: { type: 'string' },
            },
          },
          response: {
            200: {
              type: 'object',
              description: 'Successful response',
              properties: {
                id: { type: 'number' },
                name: { type: 'string' },
              },
            },
          },
        } as FastifySchema,
      },
      async (_request, _reply) => {
        const data = _request.body as { name: string };
        if (data.name) {
          const room = await roomsRepo.add(data.name);
          if (!room) {
            return _reply.status(503).send('Could not create room');
          }
          return _reply.status(200).send(room);
        } else {
          return _reply.status(400).send('Invalid input');
        }
      },
    );
    done();
  };
};
