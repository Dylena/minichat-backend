/* eslint-disable @typescript-eslint/no-misused-promises */
/* eslint-disable @typescript-eslint/no-floating-promises */
import fastify from 'fastify';
import fastifyOAS from 'fastify-oas';
import { createRoomsRepo } from './repos/rooms.repo';
import { createDb } from './database';
import { createRouter } from './api/api';
import { createMessagesRepo } from './repos/messages.repo';
import { config } from './config';
import { createUsersRepo } from './repos/users.repo';
import passport from 'fastify-passport';
import passportLocal from 'passport-local';
import fastifySecureSession from 'fastify-secure-session';
import bcrypt from 'bcrypt';
import { createDb as cdb, migrate } from 'postgres-migrations';
import path from 'path';
import fastifyCors from 'fastify-cors';
import fastifySocketIO from 'fastify-socket.io';
import { Server } from 'socket.io';
import { createSocket } from './socket';

const ONE_DAY = 86400000;

export const start = async (): Promise<void> => {
  const app = fastify({ logger: true });
  await app.register(fastifyCors, { origin: config.frontendUrl });
  await app.register(fastifySocketIO, { cors: { origin: '*' } });

  console.log('app started');
  const io = app.io as Server;
  const socket = createSocket(io);

  console.log({ io });

  io.on('connect', socket => {
    console.log((socket.client as any).id, 'connected');

    // socket.on('send-message', (data: any) => {
    //   console.log({ data });
    //   io.emit('chat', data);
    // });

    socket.on('disconnect', () => {
      console.log((socket.client as any).id, 'disconnected');
    });
  });
  // let count = 0;
  // setInterval(() => {
  //   console.log({ count });
  //   socket.sendMessage(`${count++}`);
  // }, 5000);

  const pool = await createDb(config.pg);

  app.setErrorHandler((error, _, reply) => {
    console.log(error);
    if (error.validation) {
      reply.status(422).send({
        ...error.validation,
      });
      return;
    }
    void reply.status(500).send({
      errorCode: 'INTERNAL_SERVER_ERROR',
    });
  });

  const roomsRepo = createRoomsRepo(pool);
  const messagesRepo = createMessagesRepo(pool);
  const usersRepo = createUsersRepo(pool);

  passport.use(
    'local',
    new passportLocal.Strategy(
      {
        usernameField: 'email',
        passwordField: 'password',
      },
      async (email, password, cb) => {
        const user = await usersRepo.getUserByEmail(email);
        if (!user) {
          return cb(null, false, { message: 'Insufficient permission' });
        }
        const { salt, passwordHash } = user;
        const comparePassword = await bcrypt.hash(password, salt).catch(() => null);
        if (passwordHash !== comparePassword) {
          return cb(null, false, { message: 'Insufficient permission' });
        }
        return cb(null, user);
      },
    ),
  );

  void passport.registerUserSerializer(async (user: { id: number }) => user.id);
  void passport.registerUserDeserializer(async id => usersRepo.getUserById(id as number));

  void app.register(fastifySecureSession, {
    key: Buffer.from(config.secretKey, 'hex'),
    cookieName: 'chatCookie',
    cookie: { path: '/', maxAge: ONE_DAY },
  });

  const client = await pool.connect();
  try {
    await cdb(config.databaseName, { client });
    await migrate({ client }, path.join(__dirname, '..', 'migrations'));
  } catch (e) {
    console.error(e);
    process.exit(1);
  } finally {
    client.release();
  }

  void app.register(passport.initialize());
  void app.register(passport.secureSession());

  app.log.info('Fasitfy Router done');

  void app.register(fastifyOAS, {
    routePrefix: '/documentation',
    swagger: {
      info: {
        title: 'Config Micro API',
        description: 'api documentation',
        version: '0.1.0',
      },
      externalDocs: {
        url: 'https://swagger.io',
        description: 'Find more info here',
      },
      consumes: ['application/json'],
      produces: ['application/json'],
      tags: [{ name: 'rooms', description: 'Rooms endpoints' }],
      host: 'localhost:8080',
      schemes: ['http'],
    },
    exposeRoute: true,
  });

  app.log.info('Fasitfy OAS done');

  const router = createRouter({ roomsRepo, messagesRepo, usersRepo });
  void app.register(router, { prefix: '/api' });

  app.listen(config.port, err => {
    if (err) {
      app.log.error(err);
      process.exit(1);
    }
    app.log.info('server listening');
  });
};

start();
