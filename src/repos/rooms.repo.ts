import { Pool, QueryResult } from 'pg';
import { IRoom } from '../interfaces/room';

export type RoomsRepo = {
  list: () => Promise<QueryResult<IRoom[]>>;
  get: (roomId: string) => Promise<QueryResult<IRoom>>;
  add: (name: string) => Promise<QueryResult<IRoom>>;
};

export const createRoomsRepo = (pool: Pool): RoomsRepo => {
  return {
    list: async () => pool.query('SELECT * FROM rooms'),
    get: async (roomId: string) => pool.query('SELECT * FROM rooms WHERE id=$1', [roomId]),
    add: async (name: string) => {
      const result = await pool.query('INSERT INTO rooms(name) VALUES ($1) RETURNING *;', [name]);
      return result.rows[0];
    },
  };
};
