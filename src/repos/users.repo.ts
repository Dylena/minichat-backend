import { Pool } from 'pg';
import { IUser } from '../interfaces/user';

export type UsersRepo = {
  getUserById: (id: number) => Promise<IUser>;
  getUsers: () => Promise<IUser[]>;
  getUserByEmail: (email: string) => Promise<IUser>;
  createUser: (name: string, email: string, passwordHash: string, salt: string) => Promise<IUser>;
};

export const createUsersRepo = (pool: Pool): UsersRepo => {
  return {
    getUserById: async (id: number) => {
      const result = await pool.query('SELECT * FROM users WHERE id = $1', [id]);
      return result.rows[0] as IUser;
    },
    getUsers: async () => {
      const result = await pool.query('SELECT * FROM users');
      return result.rows as IUser[];
    },
    getUserByEmail: async (email: string) => {
      const result = await pool.query('SELECT * FROM users WHERE email = $1', [email]);
      return result.rows[0] as IUser;
    },
    createUser: async (name: string, email: string, passwordHash: string, salt: string) => {
      const result = await pool.query(
        'INSERT INTO users(name, email, "passwordHash", salt) VALUES ($1, $2, $3, $4) RETURNING *;',
        [name, email, passwordHash, salt],
      );
      return result.rows[0];
    },
  };
};
