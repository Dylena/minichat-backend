import { Pool, QueryResult } from 'pg';
import { IMessage } from '../interfaces/message';
import { IRoom } from '../interfaces/room';

export type MessagesRepo = {
  get: (roomId: number) => Promise<QueryResult<IRoom>>;
  add: (roomId: number, message: IMessage) => Promise<QueryResult<IMessage>>;
};

export const createMessagesRepo = (pool: Pool): MessagesRepo => {
  return {
    get: async (roomId: number) =>
      pool.query(
        'SELECT messages.id, sender, data, date FROM messages INNER JOIN rooms ON messages.room=rooms.id WHERE room=$1;',
        [roomId],
      ),
    add: async (roomId: number, message: IMessage) => {
      message.date = new Date().toLocaleString();
      console.log(roomId, message);
      const result = await pool.query(
        'INSERT INTO messages(room, sender, data, date) VALUES ($1, $2, $3, $4) RETURNING *;',
        [roomId, message.sender, message.data, message.date],
      );
      return result.rows[0];
    },
  };
};
