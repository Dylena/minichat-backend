/* eslint-disable @typescript-eslint/interface-name-prefix */
export interface IRoom {
  id: number;
  name: string;
}
