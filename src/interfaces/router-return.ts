import { FastifyInstance } from 'fastify';

export type RouterReturn = (fastify: FastifyInstance, opts: unknown, done: () => void) => void;
