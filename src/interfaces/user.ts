// eslint-disable-next-line @typescript-eslint/interface-name-prefix
export interface IUser {
  id: number;
  email: string;
  salt: string;
  passwordHash: string;
}
