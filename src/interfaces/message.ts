/* eslint-disable @typescript-eslint/interface-name-prefix */
export interface IMessage {
  id?: number;
  data: string;
  date?: Date | string;
  sender: string; //userId
}
