import convict from 'convict';
import dotenv from 'dotenv';

dotenv.config();
dotenv.config({
  path: `.env${process.env.NODE_ENV ? `.${process.env.NODE_ENV}` : ''}`,
});

const convictConfig = convict({
  port: {
    doc: 'The port to bind.',
    format: Number,
    default: 8080,
    env: 'PORT',
  },
  pg: {
    doc: 'Postgresql connection string',
    format: String,
    default: 'postgres://user:pw@localhost:5432/chatdb',
    env: 'PG_CONNECTION_STRING',
  },
  secretKey: {
    doc: 'Secret key',
    format: String,
    default: '1b4f65facfa80b98bbdd0b747985807f6e49ce97a2fe71aa62f2aa966ef89db0',
    env: 'SECRET_KEY',
  },
  frontendUrl: {
    doc: 'frontend url',
    format: String,
    default: 'http://localhost:4200',
    env: 'FRONTEND_URL',
  },
  databaseName: {
    doc: 'database name',
    format: String,
    default: 'chatdb',
    env: 'DATABASE_NAME',
  },
});
convictConfig.validate({ allowed: 'strict' });

export const config = convictConfig.getProperties();
