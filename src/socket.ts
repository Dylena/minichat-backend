import { Server } from 'socket.io';

export const createSocket = (io: Server) => {
  return {
    sendMessage: (message: string) => {
      io.emit('chat', message);
    },
  };
};
